# CS335-GROUP6-ONLINE-BUS-TICKETING-SYSTEM
This is a Bus Ticketing System set up using the Spring Framework. Some of the features include creating an account, searching for trips, logging in, and booking trips. The program uses features of the Spring Framework such as Dependecy Injection and Spring Security. The program also uses Hibernate in order to do operations on the database.

###Setup:

   Install Prerequisite Software:
   
       Java jdk 15
       MySql Server
       IntelliJ

   
